module Main where

import Control.Monad (replicateM)
import Data.Functor.Foldable (Fix(..), cata)

data LamF a
  = Var String
  | App a a
  | Abs String a
  deriving (Functor, Foldable, Show)

type Lam = Fix LamF

showLam :: Lam -> String
showLam = cata $ \case
  Var v -> '$' : v
  App l r -> '`' : l ++ r
  Abs v e -> '^' : v ++ "." ++ e

var :: String -> Lam
var v = Fix (Var v)

app :: Lam -> Lam -> Lam
app l r = Fix (App l r)

abstr :: String -> Lam -> Lam
abstr v e = Fix (Abs v e)

alphaConvert :: Lam -> Lam
alphaConvert = snd . alphaConvert'
  where alphaConvert' :: Lam -> (Int, Lam)
        alphaConvert' = cata $ \case
          Var v -> (0, var v)
          App (m, l) (n, r) -> (max m n, app l r)
          Abs v (k, e) -> let name = concatMap (`replicateM` ['a'..'z']) [1..] !! k
                           in (k+1, abstr name (replaceWith v name e))

replaceWith :: String -> String -> Lam -> Lam
replaceWith v new = cata $ \case
  Var v' | v == v' -> var new
  x -> Fix x

eval :: Lam -> Lam
eval = cata $ \case
  App (Fix (Abs v e')) e -> eval (betaReduce v e e')
  x -> Fix x

betaReduce :: String -> Lam -> Lam -> Lam
betaReduce v e' = cata $ \case
  Var v' | v == v' -> e'
  x -> Fix x

parseLam :: String -> Maybe (String, Lam)
parseLam ('^':v:'.':s) = do
  (s', e) <- parseLam s
  pure (s', abstr [v] e)
parseLam ('`':s) = do
  (s', l) <- parseLam s
  (s'', r) <- parseLam s'
  pure (s'', app l r)
parseLam ('$':v:s) = pure (s, var [v])
parseLam _ = Nothing

main :: IO ()
main = putStrLn . showLam $ eval . alphaConvert $ app (abstr "a" $ var "b" `app` var "a") (abstr "y" $ var "y")
