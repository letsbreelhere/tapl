module FOmega.TypeCheck where

import FOmega.Types

guard :: Bool -> a -> Either a ()
guard b s = if b then pure () else Left s

typeCheck :: (Show tv, Show a, Eq a, Eq tv, Ord tv, Enum tv) => [(tv, Kind)] -> [(a, LamType tv)] -> Lam tv a -> Either String (LamType tv)
typeCheck _ _ (Bool _) = pure BoolTy
typeCheck _ _ Unit = pure UnitTy
typeCheck _ cxt (Var v) = maybe (Left $ "Couldn't find variable " ++ show v) pure $ lookup v cxt
typeCheck kCxt cxt (Abs v ty e) = do
  kind <- kindCheck kCxt ty
  guard (kind == Proper) "Lambda variable type was not proper"
  resultTy <- typeCheck kCxt ((v,ty):cxt) e
  pure (TArr ty resultTy)
typeCheck kCxt cxt (App l r) = do
  lType <- typeCheck kCxt cxt l
  rightTy <- typeCheck kCxt cxt r
  case lType of
    TArr argTy resultTy -> do
      guard (argTy `equiv` rightTy) $ "Argument type was " ++ show rightTy ++ " but expected " ++ show argTy
      pure resultTy
    _ -> Left $ "Got non-function type on left side of application: " ++ show lType
typeCheck kCxt cxt (AbsTy tv kind l) = Forall tv kind <$> typeCheck ((tv, kind):kCxt) cxt l -- TODO: Check if tv occurs in cxt, freshen?
typeCheck kCxt cxt (AppTy e ty) = do
  exprType <- typeCheck kCxt cxt e
  case evalType exprType of
    Forall tv _ generalType -> pure (generalType `applyTVar` (tv, ty))
    _ -> Left $ "Can't apply type to non-universal " ++ show (evalType exprType)
typeCheck kCxt cxt ImplEx {implType, implTerm, exTv, exKind, exType} = do
  let substType = exType `applyTVar` (exTv, implType)
  implTermType <- typeCheck ((exTv, exKind):kCxt) cxt implTerm
  guard (evalType substType `equiv` evalType implTermType) $ "Existential impl type didn't match: expected " ++ show (evalType substType) ++ " but got " ++ show (evalType implTermType)
  pure (Exists exTv exKind exType)
typeCheck kCxt cxt UseEx {letTv, letVar, letTerm, inTerm} = do
  letTermType <- typeCheck kCxt cxt letTerm
  case evalType letTermType of
    Exists exTv exKind exType -> do
      let fixedExType = exType `applyTVar` (exTv, TVar letTv)
      checked <- typeCheck ((letTv,exKind):kCxt) ((letVar,fixedExType):cxt) inTerm
      guard (letTv `notElem` checked) $ "Type variable " ++ show letTv ++ " appeared free when unpacking existential: " ++ show checked
      pure checked
    _ -> Left $ "Tried to unpack term that wasn't packed: " ++ show (evalType letTermType)

equiv :: (Eq tv) => LamType tv -> LamType tv -> Bool
equiv (Forall tv k ty) (Forall tv' k' ty') =
  k == k' && ty `equiv` (ty' `replaceTv` (tv', tv))
equiv (Exists tv k ty) (Exists tv' k' ty') =
  k == k' && ty `equiv` (ty' `replaceTv` (tv', tv))
equiv (KLam tv k ty) (KLam tv' k' ty') =
  k == k' && ty `equiv` (ty' `replaceTv` (tv', tv))
equiv (KApp l r) (KApp l' r') =
  l `equiv` l' && r `equiv` r'
equiv (TArr l r) (TArr l' r') =
  l `equiv` l' && r `equiv` r'
equiv t t' = t == t'

replaceTv :: Eq tv => LamType tv -> (tv, tv) -> LamType tv
replaceTv ty (tv, tv')= fmap (\v -> if v == tv then tv' else v) ty

kindCheck :: (Eq tv, Show tv) => [(tv, Kind)] -> LamType tv -> Either String Kind
kindCheck _ BoolTy = pure Proper
kindCheck _ UnitTy = pure Proper
kindCheck cxt (TVar tv) = maybe (Left $ "Couldn't find type variable " ++ show tv ++ " in context " ++ show cxt) pure $ lookup tv cxt
kindCheck cxt (TArr l r) = do
  lKind <- kindCheck cxt l
  rKind <- kindCheck cxt r
  guard (lKind == Proper && rKind == Proper) $ "Expected left and right kinds of type arrow to be proper, were " ++ show lKind ++ " and " ++ show rKind
  pure Proper
kindCheck cxt (KLam tv kind ty) = do
  resultKind <- kindCheck ((tv,kind):cxt) ty
  pure (KArr kind resultKind)
kindCheck cxt (KApp l r) = do
  leftKind <- kindCheck cxt l
  rightKind <- kindCheck cxt r
  case leftKind of
    KArr argKind resultKind -> do
      guard (argKind == rightKind) $ "Left side of type application was " ++ show argKind ++ ", expected " ++ show rightKind
      pure resultKind
    _ -> Left $ "Left side of type application was " ++ show leftKind ++ ", expected arrow"
kindCheck cxt (Forall tv kind ty) = do
  kind' <- kindCheck ((tv,kind):cxt) ty
  guard (kind' == Proper) $ "Expected kind of " ++ show ty ++ " to be " ++ show Proper ++ " but was " ++ show kind
  pure Proper
kindCheck cxt (Exists tv k ty) = kindCheck ((tv,k):cxt) ty

evalType :: (Eq tv, Ord tv, Enum tv) => LamType tv -> LamType tv
evalType t = case t of
  KApp l r -> reduceType (KApp (evalType l) (evalType r))
  KLam tv kind ty -> KLam tv kind (evalType ty)
  Forall tv kind ty -> Forall tv kind (evalType ty)
  Exists tv kind ty -> Exists tv kind (evalType ty)
  TArr l r -> TArr (evalType l) (evalType r)
  ty -> ty

reduceType :: (Eq tv, Ord tv, Enum tv) => LamType tv -> LamType tv
reduceType (KApp (KLam tv _ ty) r) = ty `applyTVar` (tv,r)
reduceType ty = ty

applyTVar :: (Eq tv, Ord tv, Enum tv) => LamType tv -> (tv, LamType tv) -> LamType tv
applyTVar BoolTy _ = BoolTy
applyTVar UnitTy _ = UnitTy
applyTVar var@(TVar tv) (tv', ty)
  | tv == tv' = ty
  | otherwise = var
applyTVar (TArr l r) s = TArr (applyTVar l s) (applyTVar r s)
applyTVar (Forall tv kind ty) (tv', ty')
  | tv == tv' = Forall tv kind ty
  | otherwise = let freshVar = succ $ maximum (TArr ty ty')
                    ty'' = fmap (\t -> if t == tv then freshVar else t) ty
                 in Forall freshVar kind (applyTVar ty'' (tv', ty'))
applyTVar (KLam tv kind ty) (tv', ty')
  | tv == tv' = KLam tv kind ty
  | otherwise = let freshVar = succ (max (maximum ty) (maximum ty'))
                    ty'' = fmap (\t -> if t == tv then freshVar else t) ty
                 in KLam freshVar kind (applyTVar ty'' (tv', ty'))
applyTVar (KApp l r) (tv, ty) =
  KApp (applyTVar l (tv, ty)) (applyTVar r (tv, ty))
applyTVar (Exists tv kind ty) (tv', ty')
  | tv == tv' = Exists tv kind ty
  | otherwise = let freshVar = succ (max (maximum ty) (maximum ty'))
                    ty'' = fmap (\t -> if t == tv then freshVar else t) ty
                 in Exists freshVar kind (applyTVar ty'' (tv', ty'))
