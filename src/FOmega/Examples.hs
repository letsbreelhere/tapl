module FOmega.Examples where

import FOmega.Types
import Types.Name

first :: Lam TyName Name
first =
  AbsTy alpha Proper $
  AbsTy beta Proper $
  Abs "p" pair
    (App
      (Var "p" `AppTy` TVar alpha)
      (Abs "f" (TVar alpha)
        (Abs "s" (TVar beta)
          (Var "f")
        )
      )
    )
  where
    pair =
      Forall gamma Proper $
      (TVar alpha `TArr` (TVar beta `TArr` TVar gamma)) `TArr` TVar gamma

pairInterface :: LamType TyName
pairInterface =
  let p = TyName 15
      twoOp = Proper `KArr` (Proper `KArr` Proper)
      piAlphaBeta = TVar p `KApp` TVar alpha `KApp` TVar beta
   in Exists p twoOp $
        Forall alpha Proper $
        Forall beta Proper $
          TVar alpha `TArr` (TVar beta `TArr` piAlphaBeta)

implPair :: Lam TyName Name
implPair =
  let churchPairType =
        KLam alpha Proper $
        KLam beta Proper $
          Forall gamma Proper $
            (TVar alpha `TArr` (TVar beta `TArr` TVar gamma)) `TArr` TVar gamma
      churchPairTerm =
        AbsTy alpha Proper $
        AbsTy beta Proper $
          Abs "a" (TVar alpha) $
          Abs "b" (TVar beta) $
            AbsTy gamma Proper $
              Abs "f" (TVar alpha `TArr` (TVar beta `TArr` TVar gamma)) $
                Var "f" `App` Var "a" `App` Var "b"
      p = TyName 15
      piAlphaBeta = TVar p `KApp` TVar alpha `KApp` TVar beta
   in ImplEx
     { implType = churchPairType
     , implTerm = churchPairTerm
     , exTv = p
     , exKind = Proper `KArr` (Proper `KArr` Proper)
     , exType =
        Forall alpha Proper $
        Forall beta Proper $
          TVar alpha `TArr` (TVar beta `TArr` piAlphaBeta)
     }

usePair :: Lam TyName Name
usePair =
  let p = TyName 15
  in UseEx
     { letTv   = p
     , letVar  = "mk"
     , letTerm = implPair
     , inTerm  = Var "mk" `AppTy` BoolTy `AppTy` BoolTy `App` Bool True `App` Bool False
     }
