* Introduction
* The Untyped Lambda Calculus, λ
  * Beta Reduction
  * Alpha Conversion
  * A Simple Parser and REPL
* The Simply-Typed Lambda Calculus, λ⭢
  * Annotating Expressions
  * Typing Rules and Notation
    * Introduction vs Elimination
  * Typechecking
* Polymorphic Types and System F
  * Data Types the Hard Way: Church Encoding
  * Basic Type Inference
  * Kinds
* Type Operators and λω̲
* Dependent Types and λΠ
  * What About Inference?
* Putting it All Together: the Calculus of Constructions
* Typeclasses
* Subtyping and Pure Objects
* Substructural Typing and Resource Management
