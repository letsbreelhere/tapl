# Introduction

Haskell is often considered a "type laboratory": it has both a fairly
advanced type system and one that is relatively easy to extend. In
part because of this, there is recently an entire sub-community of
functional programmers who are deeply interested in type systems and the
implementation of statically typed languages.

Unfortunately, the resources for a programmer interested in diving
into type theory are relatively scattered - a few textbooks covering
the basics, with many more short and abstruse white papers covering
more interesting material piecemeal. Both can be fairly daunting for a
newcomer less acquainted with mathematical jargon and notation usually
associated with type theory academia. What's a budding type theory
newbie to do?

The goal of this series, in the spirit of Bartosz Milewski's Categories
for Programmers blog posts, is to work through the various problems of
type theory incrementally, giving implementations in Haskell along the
way. My hope is to work through untyped lambda calculus into System
F, in roughly the order laid out by the book Types and Programming
Languages. Ideally, I will be able to pull in a few ideas from the
sequel, Advanced Topics in Types and Programming Languages, as well.

## Prerequisites

* Understanding of basic lambda calculus Intermediate Haskell knowledge

## Structure

When we talk about static functional type systems, we are
usually talking about various extensions made to the untyped
lambda calculus. The most common of these can be arranged in the
often-referenced Lambda Cube, which assign the three axes to three
independent refinements of this very simple calculus, leading
ultimately to the very powerful (and equally complex) Calculus of
Constructions. We will first implement the untyped lambda calculus with
a parser and interpreter REPL that will act as a sandbox for testing
our assumptions. Afterward, we will separately implement each axis on
the Lambda Cube on top of our original: polymorphism, type operators,
and dependent types. We will then discuss combining these typing rules,
which yields the four calculi on the far end of the cube, ending in the
Calculus of Constructions similar to the system used by languages such
as Coq.

We will also look at a few refinements not included on the Lambda Cube,
in particular subtyping, substructural typing, and typeclasses. Along
the way we will implement type inference for these calculi where
possible.
