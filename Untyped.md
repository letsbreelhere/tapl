# The Untyped Lambda Calculus

Programmers often talk about "the" lambda calculus as if it had a single
fixed formal definition or structure. This is a simplification, of
course - since its introduction, a number of lambda calculi have been
developed in order to make certain kinds of computation clearer and more
concise.

All functional programming, historically and theoretically, has
descended from the lambda calculus introduced by Alonzo Church's lambda
calculus. Church introduced the calculus in the 1930s in an (ultimately
unsuccessful) attempt to formalize the foundations of logic. This fact
is often glossed over in introductions to the calculus, but is pretty
bizarre on its face. How does a failed tool from formal logic become
widely known as the basis of an entire programming language paradigm?

While this fact might seem surprising, the explanation behind it
is an extraordinarily profound result called the Curry-Howard
isomorphism. Roughly speaking, the isomorphism identifies the terms of
various lambda calculi to proofs in various systems of logic (which
system depends on which calculus), and the types of those terms to
propositions. As we explore various type systems through the course of
this series, I will include a higher-level explanation of the logic that
a given type system corresponds to.

The rest of this chapter is a review of the untyped lambda calculus, along with an implementation of an evaluator.

## The Basic Datatype

Lambda calculus expressions are extremely simple: we can only reference
a variable, bind a variable with a λ-abstraction, and apply one
expression to another.

```haskell
data Lam a
  = Var a
  | Abs a Lam
  | App Lam Lam
  deriving (Functor, Foldable)
```

Evaluation is equally simple: the only computational step we can perform
is to _β-reduce_ an application, where we replace the bound variable on the left with the expression on the right. The rest is simply recursion into the rest of the structure.

```haskell
eval :: Eq a => Lam a -> Lam a
eval (App l r) = case eval l of
                   Abs v e -> eval (betaReduce v (eval r) e)
                   l' -> App l' (eval r)
eval (Abs v e) -> Abs v (eval e)
eval (Var v) -> Var v

betaReduce :: Eq a => a -> Lam a -> Lam a -> Lam a
betaReduce v e (App l r) = App (betaReduce v e l) (betaReduce v e r)
betaReduce v e (Abs v' e') = Abs v' (betaReduce v e e')
betaReduce v e (Var v')
  | v == v' = e
  | otherwise = Var v'
```

This is nearly all that is needed to fully implement the untyped calculus. We're left with one problem, namely that variable replacement can behave incorrectly when two abstractions using the same variable interact. For example:

```haskell
λ> eval $ App (Abs "b" (Abs "a" (Var "b"))) (Var "a")
=> Abs "a" (Var "a")
```

This is incorrect - what should be a constant function reduces to the identity instead. To fix this we need to supply variable names on the right that are unused on the left. Using integers as variable names, we can increment all right-hand variables past the largest name used on the left:

```haskell
useFresh :: Lam Int -> Lam Int
useFresh (App l r) = let l' = useFresh l
                         maxLeft = maximum l'
                         r' = fmap (maxLeft +) (useFresh r)
                      in App l' r'
useFresh e = e
```
